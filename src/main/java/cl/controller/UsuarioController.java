/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.util.Usuario;
import com.dao.FormulariocovidJpaController;
import com.dao.exceptions.NonexistentEntityException;
import com.entity.Formulariocovid;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @Author : Juan Pablo Arriagada
 */
@WebServlet(name = "UsuarioController", urlPatterns = {"/UsuarioController"})
public class UsuarioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UsuarioController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UsuarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session=request.getSession();
        Usuario usuario= (Usuario)session.getAttribute("usuario");
        
        FormulariocovidJpaController dao =new FormulariocovidJpaController();
        System.out.println("usuario.getNombre():"+usuario.getNombre());
        Formulariocovid formulario=dao.findFormulariocovid(usuario.getNombre());
       
        request.setAttribute("formulario", formulario);
        request.getRequestDispatcher("infousuario.jsp").forward(request, response);  
        
    
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        String rut = request.getParameter("rut");
        String nombres = request.getParameter("nombres");
        String apellido_paterno = request.getParameter("apellido_paterno");
        String apellido_materno = request.getParameter("apellido_materno");
        String correo_electronico = request.getParameter("correo_electronico");
        String accion = request.getParameter("accion");
        Formulariocovid formulario = new Formulariocovid();

        formulario.setRut(rut);
        formulario.setNombres(nombres);
        formulario.setApellidoPaterno(apellido_paterno);
        formulario.setApellidoMaterno(apellido_materno);
        formulario.setCorreoElectronico(correo_electronico);

        FormulariocovidJpaController dao =new FormulariocovidJpaController();

        if (accion.equals("modificar")) {

            try {
                dao.edit(formulario);
            } catch (Exception ex) {
                Logger.getLogger(Formulariocovid.class.getName()).log(Level.SEVERE, null, ex);
            }

            request.setAttribute("formulario", formulario);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
         if (accion.equals("eliminar")) {
             
            try {
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(Formulariocovid.class.getName()).log(Level.SEVERE, null, ex);
            }
              request.getRequestDispatcher("index.jsp").forward(request, response);  
         }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
