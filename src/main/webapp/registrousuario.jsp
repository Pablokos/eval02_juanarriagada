<%-- 
    Document   : regitrousuario
    Author     : Juan Pablo Arriagada
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Página de Inscripción</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"> 
            <style>
            *{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
            body{margin:3px; padding:5px; font-family: 'Open Sans', sans-serif;}
            section{width:100%; max-width:2000px; margin:0px auto; display:table; position:relative;}
            h2{margin:1px auto;  font-size:34px; padding:30px 0px; color:#002e5b; text-align:left;}
            h2 span{font-weight:500;}

            header{width:100%; display:table; background-color:#FFFFFF; margin-bottom:50px;}
            #logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
        </style>
    </head>
    <body>
        <h1>Bienvenido, complete sus datos para registrarse</h1>

        <form name="form" action="FormularioController" method="POST">
            <table >
                <tr>
                    <td height="25">&nbsp;</td>
                </tr>
                <tr >
                    <td width="250"><b>Ingrese Rut</b></td><!--Celda de la etiqueta nombre-->
                    <td><input type="text" name="rut" id="rut"></td>
                </tr>
                <tr>
                    <td height="25">&nbsp;</td>
                </tr>
                <tr>
                    <td width="250"><b>Ingrese Nombres</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="nombres" id="nombres"></td>
                </tr>
                <tr>
                    <td height="25">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td width="250"><b>Ingrese Apellido Paterno</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="apellido_paterno" id="apellido_paterno"></td>
                </tr>
                <tr>
                    <td height="25">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td width="250"><b>Ingrese Apellido Materno</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="apellido_materno" id="apellido_materno"></td>
                </tr>
                <tr>
                    <td height="25">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td width="250"><b>Ingrese Correo electrónico</b></td><!--Celda de la etiqueta Sección-->
                    <td><input type="text" name="correo_electronico" id="correo_electronico"></td>
                </tr>  
                <tr>
                    <td height="45">&nbsp;</td><!--fila de espacio-->
                </tr>
                <tr>
                    <td>
                        <button type="submit" name="accion" value="ingresar" class="btn btn-success" onclick="alert('Registrado correctamente');">Ingresar Solicitud</button>
                        
                    </td>
                </tr>
            </table> 
        </form> 
    </body>
</html>