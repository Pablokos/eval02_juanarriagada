<%-- 
    Document   : infousuario
    Author     : Juan Pablo Arriagada
--%>

<%@page import="cl.util.Usuario"%>
<%@page import="com.entity.Formulariocovid"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    HttpSession session2 = request.getSession();

    Usuario usuario = (Usuario) session2.getAttribute("usuario");

    if (usuario == null) {

        request.getRequestDispatcher("index.jsp").forward(request, response);

    }
    Formulariocovid formulario = (Formulariocovid) request.getAttribute("formulario");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"> 
        <style>
            *{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
            body{margin:3px; padding:5px; font-family: 'Open Sans', sans-serif;}
            section{width:100%; max-width:2000px; margin:0px auto; display:table; position:relative;}
            h2{margin:1px auto;  font-size:34px; padding:30px 0px; color:#002e5b; text-align:left;}
            h2 span{font-weight:500;}

            header{width:100%; display:table; margin-bottom:50px;}
            #logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
        </style>
    </head>
    <body>
        <header>

            <a href="yur" id="logo" target="_blank"></a>

            <label for="toggle-1" class="toggle-menu"><ul><li></li> <li></li> <li></li></ul></label>


            <section id="about" class="content">

                <h2>Datos Registrados</h2>
                <p>


                <form  name="form" action="UsuarioController" method="POST">
                    <% if ((usuario != null) && (formulario != null)) {%>

                    <table >
                        <tr >  
                        <div class="form-group">
                            <td width="150"><b>Rut</b></td>
                            <td><input  name="rut" value="<%= formulario.getRut()%>"  class="form-control" required id="rut" aria-describedby="usernameHelp"></td>
                        </div>
                        </tr> 
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>

                        <tr >    
                        <div class="form-group">
                            <td width="100"><b>Nombres</b></td>
                            <td><input  step="any" name="nombres" value="<%= formulario.getNombres()%>"  class="form-control" required id="nombres" aria-describedby="nombreHelp"></td>
                        </div>       
                        </tr>
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>
                        <tr >
                        <div class="form-group">
                            <td width="100"><b>Apellido Paterno</b></td>
                            <td><input  step="any" name="apellido_paterno" value="<%= formulario.getApellidoPaterno()%> "  class="form-control" required id="apellido_paterno" aria-describedby="nombreHelp"></td>
                        </div> 
                        </tr> 
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>
                        <tr >
                        <div class="form-group">
                            <td width="100"><b>Apellido Materno</b></td>
                            <td><input  step="any" name="apellido_materno" value="<%= formulario.getApellidoMaterno()%> "  class="form-control" required id="apellido_materno" aria-describedby="nombreHelp"></td>
                        </div> 
                        </tr> 
                        <tr>
                            <td height="15">&nbsp;</td>
                        </tr>
                        <tr >
                        <div class="form-group">
                            <td width="100"><b>Correo</b></td>
                            <td width="150"><input  step="any" name="correo_electronico" value="<%= formulario.getCorreoElectronico()%> "  class="form-control" required id="correo_electronico" aria-describedby="nombreHelp"></td>
                        </div> 
                        </tr>
                        <tr >
                            <td>
                                <br><br>
                            </td>
                        </tr>
                    </table><!-- comment -->
                    <table>
                        <tr > 
                            <td><button type="submit" name="accion" value="modificar" class="btn btn-success" onclick="alert('Se ha Modificado el registro');"> Actualizar Datos y Salir </button></td>
                            <td width="30"></td>
                            <td><button type="submit" name="accion" value="eliminar" class="btn btn-success" onclick="alert('Se ha Eliminado el registro');"> Eliminar Datos </button></td>
                        </tr> 
                    </table>

                    <%}%>

                </form>

                </p>
            </section>
        </header>


    </body>
</html>